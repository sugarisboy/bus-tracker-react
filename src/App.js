import logo from './logo.svg';
import './App.css';
import React from "react";
import {Tab, Tabs, TabList, TabPanel} from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import RoutePage, {style} from "./components/Route/RoutePage";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import {Button} from "@material-ui/core";
import StationPage from "./components/Station/StationPage";
import BusPage from "./components/Bus/BusPage";
import PassengerPage from "./components/Passenger/PassengerPage";

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            exception: {
                open: false,
                msg: ""
            }
        }
    }

    render() {
        return (
            <div className="App" style={{width: '800px', margin: '48px auto', height: '100%'}}>
                <Dialog open={this.state.exception.open} onClose={() => this.closeExceptionPopup()}>
                    <DialogContent style={{textAlign: "center"}}>
                        <div style={{fontSize: '24px', color: 'red'}}>
                            {this.state.exception.msg.split("\n").map(line => (
                                <><span>{line}</span><br/></>
                            ))}
                        </div>
                    </DialogContent>
                    <Button variant="contained" onClick={() => this.closeExceptionPopup()}><b>Закрыть</b></Button>
                </Dialog>

                <Tabs>
                    <TabList>
                        <Tab>Маршруты</Tab>
                        <Tab>Автобусы</Tab>
                        <Tab>Остановки</Tab>
                        <Tab>Пассажиры</Tab>
                    </TabList>

                    <TabPanel>
                        <RoutePage showExceptionPopup={this.showExceptionPopup} selector={false}/>
                    </TabPanel>
                    <TabPanel>
                        <BusPage showExceptionPopup={this.showExceptionPopup} selector={false}/>
                    </TabPanel>
                    <TabPanel>
                        <StationPage showExceptionPopup={this.showExceptionPopup}/>
                    </TabPanel>
                    <TabPanel>
                        <PassengerPage showExceptionPopup={this.showExceptionPopup}/>
                    </TabPanel>
                </Tabs>
            </div>
        );
    }

    showExceptionPopup = (msg) => {
        this.setState({
            ...this.state,
            exception: {
                open: true,
                msg: msg
            }
        })
    }

    closeExceptionPopup = () => {
        this.setState({
            ...this.state,
            exception: {
                ...this.state.exception,
                open: false,
            }
        })
    }
}

export default App;
