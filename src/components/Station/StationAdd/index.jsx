import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {post} from "../../../service/api";

class StationAdd extends React.Component {

    constructor(props) {
        super(props);

        this.state = this.defaultState()
    }

    defaultState() {
        return {title: ''}
    }

    close = () => {
        this.props.pageInstance.setState({
            ...this.props.pageInstance.state,
            phase: 'none'
        })
        this.setState({
            ...this.state,
            ...this.defaultState()
        })
        this.props.pageInstance.loadPage()
    }

    handleChange = e => {
        const {name, value} = e.target

        if (name === 'title') {
            if (value.length > 63) {
                this.props.showExceptionPopup("Превышен лимит символов")
                return
            }

            this.setState({
                ...this.state,
                title: value
            })
        }
    }

    handleSubmit = () => {
        const data = {
            title: this.state.title
        }
        post('/station', data)
            .then(() => this.close())
            .catch(error => {
                this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка создания')
            })
    }

    render() {
        const phase = this.props.pageInstance.state.phase;
        return (
            <>
                <Dialog open={phase === 'create'} onClose={this.close}>
                    <DialogContent style={{width: '500px'}}>
                        <h2>Создание остановки</h2>
                        <TextField value={this.state.title}
                                   name="title"
                                   label="Название остановки"
                                   onChange={this.handleChange}
                                   style={{width: '100%'}}
                        />
                        <br/>
                        <br/>
                        <Button variant="contained" onClick={this.handleSubmit}>Создать</Button>
                        <br/>
                        <br/>
                    </DialogContent>
                </Dialog>
            </>
        )

    }
}

export default StationAdd