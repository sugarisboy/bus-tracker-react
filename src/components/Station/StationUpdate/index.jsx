import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {get, patch, post} from "../../../service/api";

class StationUpdate extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            title: 'загрузка',
            id: null
        }
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.id !== this.extractId()) {
            this.loadData()
        }
    }

    extractId = () => {
        const phase = this.props.pageInstance.state.phase
        return parseInt(phase.replace("update_", ''))
    }

    loadData = () => {
        const id = this.extractId()

        if (isNaN(id))
            return

        get(`/station/${id}`)
            .then(response => {
                const {id, title} = response.data
                this.setState({
                    ...this.state,
                    id,
                    title
                })
            })
    }

    close = () => {
        this.props.pageInstance.setState({
            ...this.props.pageInstance.state,
            phase: 'none'
        })
        this.props.pageInstance.loadPage()
    }

    handleChange = e => {
        const {name, value} = e.target

        if (name === 'title') {
            if (value.length > 63) {
                this.props.showExceptionPopup("Превышен лимит символов")
                return
            }

            this.setState({
                ...this.state,
                title: value
            })
        }
    }

    handleSubmit = () => {
        const data = {
            title: this.state.title
        }
        patch(`/station/${this.state.id}`, data)
            .then(() => this.close())
            .catch(error => {
                this.props.showExceptionPopup(error.response && error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка изменения')
            })
    }

    render() {
        const phase = this.props.pageInstance.state.phase;
        return (
            <>
                <Dialog open={phase.startsWith('update')} onClose={this.close}>
                    <DialogContent style={{width: '500px'}}>
                        <h2>Изменение остановки</h2>
                        <TextField value={this.state.title}
                                   name="title"
                                   label="Название остановки"
                                   onChange={this.handleChange}
                                   style={{width: '100%'}}
                        />
                        <br/>
                        <br/>
                        <Button variant="contained" onClick={this.handleSubmit}>Изменить</Button>
                        <br/>
                        <br/>
                    </DialogContent>
                </Dialog>
            </>
        )

    }
}

export default StationUpdate