import React from 'react'
import {Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {get, del} from '../../../service/api'
import StationAdd from "../StationAdd";
import StationUpdate from "../StationUpdate";
import StationTable from "../StationTable";
import Tooltip from '@material-ui/core/Tooltip';
import StationRouteSelector from "../StationRouteSelector";

class StationPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            content: [],
            page: 0,
            str: '',
            phase: 'none',
            selected: [],
            selected2: [],
        }

        this.system = {
            urls: {
                main: '/station'
            }
        }

    }

    componentDidMount() {
        this.loadPage();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.page !== this.state.page) {
            this.loadPage()
        }
        if (prevState.str !== this.state.str) {
            this.loadPage()
        }
    }

    deleteItems = async () => {
        if (this.state.selected.length === 0) {
            this.props.showExceptionPopup('Ничего не выбрано')
        }

        await this.state.selected.forEach(id => {
            del(`${this.system.urls.main}/${id}`)
                .then(() => this.loadPage())
                .catch(error => {
                    this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                        error.response.data.message : 'Ошибка удаления')
                })
        })
    }

    updateItem = (id) => {
        this.setState({
            ...this.state,
            phase: 'update_' + id
        })
    }

    loadPage() {
        const size = 10;
        get(`${this.system.urls.main}?limit=${size}&offset=${this.state.page * size}&str=${this.state.str}`)
            .then(res => {
                this.setState({
                    ...this.state,
                    content: res.data
                })
            })
    }

    selectRoute = () => {
        if (this.state.selected.length === 0) {
            this.props.showExceptionPopup('Ничего не выбрано')
        }

        this.setState({
            ...this.state,
            phase: 'route_selector'
        })
    }

    dropSelection = () => {
        this.setState({
            ...this.state,
            selected: []
        })
    }

    render() {
        return (
            <>
                <StationAdd showExceptionPopup={this.props.showExceptionPopup} pageInstance={this} selector={this.props.selector}/>
                <StationUpdate showExceptionPopup={this.props.showExceptionPopup} pageInstance={this} selector={this.props.selector}/>
                <StationRouteSelector showExceptionPopup={this.props.showExceptionPopup} pageInstance={this} selector={this.props.selector}/>

                <div style={stylePage.div}>
                    <div style={{textAlign: 'right'}}>
                        <TextField label="Поиск по названию" fullWidth onChange={this.handleFilterStr}/>
                    </div>

                    <StationTable content={this.state.content} pageInstance={this} selector={this.props.selector}/>

                    <div style={{textAlign: 'right'}}>

                        <div>
                            {this.props.selector ?
                                <>
                                    <Button variant="contained"
                                            onClick={() => this.props.handleSubmit(this.state.selected, this.state.selected2)}
                                            style={stylePage.button}>
                                        Выбрать
                                    </Button>
                                </>
                                :
                                <>
                                    <Tooltip
                                        title="Добавляет выбранные остановки в порядке выбора в конец списка остановок маршрута.
                                   Изменить порядок остановок можно будет во вкладке маршруты, в режиме редактирования"
                                        aria-label="add"
                                    >
                                        <Button variant="contained"
                                                style={stylePage.button}
                                                onClick={() => this.selectRoute()}
                                        >
                                            Привязать к маршруту
                                        </Button>
                                    </Tooltip>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <Button variant="contained" onClick={() => this.setPhase('create')}
                                            style={stylePage.button}>
                                        ╂
                                    </Button>

                                    <Button variant="contained" onClick={() => this.deleteItems()}
                                            style={stylePage.button}>
                                        ╳
                                    </Button>
                                </>
                            }
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <Button variant="contained" onClick={() => this.leftPage()} style={stylePage.button}>←</Button>
                            {this.state.page + 1}
                            <Button variant="contained" onClick={() => this.rightPage()} style={stylePage.button}>→</Button>
                        </div>
                        <div>
                            <Button variant="contained"
                                    onClick={() => this.dropSelection()}
                                    style={stylePage.button}>
                                Сбросить выделение
                            </Button>
                        </div>

                    </div>
                </div>
            </>
        )
    }

    setPhase(phase) {
        this.setState({
            ...this.state,
            phase: phase
        })
    }

    rightPage() {
        const {content} = this.state

        if (content.length < 10) {
            this.props.showExceptionPopup("Показана последняя страница")
            return
        }

        this.setState({
            ...this.state,
            page: this.state.page + 1
        })
    }

    leftPage() {
        if (this.state.page <= 0) {
            this.props.showExceptionPopup("Показана первая страница")
            return
        }

        this.setState({
            ...this.state,
            page: this.state.page - 1
        })
    }

    handleFilterStr = e => {
        const {value} = e.target

        this.setState({
            ...this.state,
            str: value,
            page: 0
        })
    }
}

export const stylePage = {
    div: {
        textAlign: 'left',
        display: 'flex',
        flexDirection: 'column'
    },
    button: {
        margin: '8px 4px',
    }

}


export default StationPage;