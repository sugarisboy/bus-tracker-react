import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import {Button} from "@material-ui/core";

class StationTable extends React.Component {

    setSelected = (id) => {
        const selected = this.props.pageInstance.state.selected
        if (selected.includes(id)) {
            this.props.pageInstance.state.selected = selected.filter(i => i !== id)
        } else {
            selected.push(id)
        }
        this.setState({...this.state})
    }

    setSelected2 = (id) => {
        const selected2 = this.props.pageInstance.state.selected2
        if (selected2.includes(id)) {
            this.props.pageInstance.state.selected2 = selected2.filter(i => i !== id)
        } else {
            selected2.push(id)
        }
        this.setState({...this.state})
    }

    hasSelected = (id) => {
        const selected = this.props.pageInstance.state.selected
        return selected.includes(id)
    }

    hasSelected2 = (id) => {
        const selected = this.props.pageInstance.state.selected2
        return selected.includes(id)
    }

    row = (item) => {
        const {id, title} = item
        const isSelected = this.hasSelected(id)
        const isSelected2 = this.hasSelected2(id)
        return (
            <TableRow key={item.id}>
                <TableCell align="right">
                    <Checkbox
                        style={{margin: '0 auto'}}
                        color="default"
                        checked={isSelected}
                        onChange={() => this.setSelected(id)}
                    />
                </TableCell>
                <TableCell align="right">
                    {
                        !this.props.selector ?
                            <>{id}</>
                            :
                            <Checkbox
                                style={{margin: '0 auto'}}
                                color="default"
                                checked={isSelected2}
                                onChange={() => this.setSelected2(id)}
                            />
                    }
                </TableCell>
                <TableCell align="right">{title}</TableCell>
                {!this.props.selector &&
                <TableCell align="right">
                    <Button variant="contained" onClick={() => this.props.pageInstance.updateItem(id)}>
                        🖉
                    </Button>
                </TableCell>
                }
            </TableRow>
        )
    }

    render() {
        return (

            <TableContainer component={Paper}>
                <div style={{minHeight: '600px'}}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="right">{this.props.selector && 'Откуда'}</TableCell>
                                <TableCell align="right">{!this.props.selector ? 'Id' : 'Куда'}</TableCell>
                                <TableCell align="right">Название</TableCell>
                                {!this.props.selector && <TableCell align="right">Изменить</TableCell>}
                            </TableRow>
                        </TableHead>
                        <TableBody>

                            {this.props.content && this.props.content.map(i => this.row(i))}

                        </TableBody>
                    </Table>
                </div>
            </TableContainer>
        )
    }
}

export default StationTable