import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {post} from "../../../service/api";
import RoutePage from "../../Route/RoutePage";

class StationRouteSelector extends React.Component {

    constructor(props) {
        super(props);

        this.state = this.defaultState()
    }

    defaultState() {
        return {}
    }

    close = () => {
        this.props.pageInstance.setState({
            ...this.props.pageInstance.state,
            phase: 'none'
        })
        this.setState({
            ...this.state,
            ...this.defaultState()
        })
        this.props.pageInstance.loadPage()
    }

    handleSubmit = (ids) => {
        const selectedStations = this.props.pageInstance.state.selected
        const selectedRoutes = ids

        selectedRoutes.forEach(routeId => {
            post(`/station/add-to-route?routeId=${routeId}`, selectedStations)
                .then(() => this.close())
                .catch(error => {
                    this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                        error.response.data.message : 'Ошибка добавления к маршруту')
                })
        })
    }

    render() {
        const phase = this.props.pageInstance.state.phase;
        return (
            <>
                <Dialog open={phase === 'route_selector'} onClose={this.close}>
                    <DialogContent style={{width: '500px'}}>
                        <RoutePage showExceptionPopup={this.props.showExceptionPopup} selector={true} handleSubmit={this.handleSubmit}/>
                    </DialogContent>
                </Dialog>
            </>
        )

    }
}

export default StationRouteSelector