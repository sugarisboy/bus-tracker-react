import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {post} from "../../../service/api";

class BusAdd extends React.Component {

    constructor(props) {
        super(props);

        this.state = this.defaultState()
    }

    defaultState() {
        return {routeId: ''}
    }

    close = () => {
        this.props.pageInstance.setState({
            ...this.props.pageInstance.state,
            phase: 'none'
        })
        this.setState({
            ...this.state,
            ...this.defaultState()
        })
        this.props.pageInstance.loadPage()
    }

    handleChange = e => {
        const {name, value} = e.target

        if (value !== '' && value.match(/^[0-9]+$/) === null) {
            return
        }

        if (name === 'routeId') {
            if (value.length > 7) {
                this.props.showExceptionPopup("Превышен лимит символов")
                return
            }

            this.setState({
                ...this.state,
                routeId: value
            })
        }
    }

    handleSubmit = () => {
        const data = {
            routeId: this.state.routeId
        }
        post('/bus', data)
            .then(() => this.close())
            .catch(error => {
                this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка добавления автобуса')
            })
    }

    render() {
        const phase = this.props.pageInstance.state.phase;
        return (
            <>
                <Dialog open={phase === 'create'} onClose={this.close}>
                    <DialogContent style={{width: '500px'}}>
                        <h2>Добавление автобуса</h2>
                        <TextField value={this.state.routeId}
                                   name="routeId"
                                   label="Введите id маршрута"
                                   onChange={this.handleChange}
                                   style={{width: '100%'}}
                        />
                        <br/>
                        <br/>
                        <Button variant="contained" onClick={this.handleSubmit}>Создать</Button>
                        <br/>
                        <br/>
                    </DialogContent>
                </Dialog>
            </>
        )

    }
}

export default BusAdd