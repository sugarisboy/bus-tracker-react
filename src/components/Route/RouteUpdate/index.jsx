import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {del, get, patch, post} from "../../../service/api";
import './index.css'

class RouteUpdate extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            title: 'загрузка',
            id: null,
            stations: []
        }
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.id !== this.extractId()) {
            this.loadData()
        }
    }

    extractId = () => {
        const phase = this.props.pageInstance.state.phase
        return parseInt(phase.replace("update_", ''))
    }

    loadData = () => {
        const id = this.extractId()

        if (isNaN(id))
            return

        get(`/route/${id}`)
            .then(response => {
                const {id, title} = response.data
                this.setState({
                    ...this.state,
                    id,
                    title
                })
            })


        get(`/station/find?routeId=${id}`)
            .then(response => {
                const stations = response.data
                this.setState({
                    ...this.state,
                    stations
                })
            })
    }

    close = () => {
        this.props.pageInstance.setState({
            ...this.props.pageInstance.state,
            phase: 'none'
        })
        this.props.pageInstance.loadPage()
    }

    handleChange = e => {
        const {name, value} = e.target

        if (name === 'title') {
            if (value.length > 7) {
                this.props.showExceptionPopup("Превышен лимит символов")
                return
            }

            this.setState({
                ...this.state,
                title: value
            })
        }
    }

    handleSubmit = () => {
        const data = {
            title: this.state.title
        }
        patch(`/route/${this.state.id}`, data)
            .then(() => this.close())
            .catch(error => {
                this.props.showExceptionPopup(error.response && error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка изменения')
            })
    }

    handleSwap = (station1, station2) => {
        const routeId = this.extractId();

        post(`/station/swap?routeId=${routeId}&station1=${station1}&station2=${station2}`)
            .then(() => this.loadData())
            .catch(error => {
                this.props.showExceptionPopup(error.response && error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка изменения порядка')
            })
    }

    handleDelete = (station) => {
        const routeId = this.extractId();

        del(`/station/remote-from-route?routeId=${routeId}&stationId=${station}`)
            .then(() => this.loadData())
            .catch(error => {
                this.props.showExceptionPopup(error.response && error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка удаления остановки')
            })
    }

    renderRowsStations() {
        const {stations} = this.state
        var prev
        return (
            <>
                {
                    stations.map((station, k) => {
                        const prevFinal = prev
                        const prevId = prev !== undefined ? prev.id : null
                        const nextFinal = k < stations.length - 1 ? stations[k + 1] : null

                        const row = (
                            <div className="station-block" key={station.id}>
                                <div className="button-station-block">
                                    <button disabled={prevId === null}
                                            className="button-level-change"
                                            onClick={() => this.handleSwap(station.id, prevFinal.id)}
                                            style={{borderRadius: '5px 5px 0 0'}}
                                    >↥
                                    </button>
                                    <button disabled={nextFinal === null}
                                            className="button-level-change"
                                            onClick={() => this.handleSwap(station.id, nextFinal.id)}
                                            style={{borderRadius: '0 0 5px 5px'}}
                                    >↧
                                    </button>
                                </div>
                                <span>{station.title}</span>
                                <div className="button-station-right">
                                    <button className="button-level-change"
                                            style={{height: '100%'}}
                                            onClick={() => this.handleDelete(station.id)}
                                    >╳</button>
                                </div>
                            </div>
                        )
                        prev = station
                        return row
                    })
                }
            </>
        )
    }

    render() {
        const phase = this.props.pageInstance.state.phase;
        return (
            <>
                <Dialog open={phase.startsWith('update')} onClose={this.close}>
                    <DialogContent style={{width: '500px'}}>
                        <h2>Изменение маршрута</h2>
                        <TextField value={this.state.title}
                                   name="title"
                                   label="Название маршрута"
                                   onChange={this.handleChange}
                                   style={{width: '100%'}}
                        />
                        <br/>
                        <br/>
                        <Button variant="contained" onClick={this.handleSubmit}>Изменить</Button>
                        <h3>Список остановок</h3>
                        {this.renderRowsStations()}
                        <br/>
                        <br/>
                    </DialogContent>
                </Dialog>
            </>
        )

    }
}

export default RouteUpdate