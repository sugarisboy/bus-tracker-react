import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {post} from "../../../service/api";
import RoutePage from "../../Route/RoutePage";
import StationPage from "../../Station/StationPage";

class PassengerStationSelector extends React.Component {

    constructor(props) {
        super(props);

        this.state = this.defaultState()
    }

    defaultState() {
        return {}
    }

    extractId = () => {
        const phase = this.props.pageInstance.state.phase
        return parseInt(phase.replace("phase_to_wait_", ''))
    }

    close = () => {
        this.props.pageInstance.setState({
            ...this.props.pageInstance.state,
            phase: 'none'
        })
        this.setState({
            ...this.state,
            ...this.defaultState()
        })
        this.props.pageInstance.loadPage()
    }

    handleSubmit = (from, to) => {
        const data  = {from, to}
        post(`/passenger/add-stations?passengerId=${this.extractId()}`, data)
            .then(() => this.close())
            .catch(error => {
                this.props.showExceptionPopup(error.response && error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка выбора остановок')
            })
    }

    render() {
        const phase = this.props.pageInstance.state.phase;
        return (
            <>
                <Dialog open={phase.startsWith('phase_to_wait_')} onClose={this.close}>
                    <DialogContent style={{width: '500px'}}>
                        <StationPage showExceptionPopup={this.props.showExceptionPopup}
                                     selector={true}
                                     handleSubmit={this.handleSubmit}
                        />
                    </DialogContent>
                </Dialog>
            </>
        )

    }
}

export default PassengerStationSelector