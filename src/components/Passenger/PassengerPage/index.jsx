import React from 'react'
import {Button} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {get, del, post} from '../../../service/api'
import PassengerAdd from "../PassengerAdd";
import PassengerUpdate from "../PassengerUpdate";
import PassengerTable from "../PassengerTable";
import StationRouteSelector from "../../Station/StationRouteSelector";
import PassengerStationSelector from "../PassengerStationSelector";

class PassengerPage extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            content: [],
            page: 0,
            str: '',
            phase: 'none',
            selected: [],
            selectedFromStations: [],
            selectedToStations: []
        }

        this.system = {
            urls: {
                main: '/passenger'
            }
        }

    }

    componentDidMount() {
        this.loadPage();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.page !== this.state.page) {
            this.loadPage()
        }
        if (prevState.str !== this.state.str) {
            this.loadPage()
        }
    }

    deleteItems = async () => {
        if (this.state.selected.length === 0) {
            this.props.showExceptionPopup('Ничего не выбрано')
        }

        await this.state.selected.forEach(id => {
            del(`${this.system.urls.main}/${id}`)
                .then(() => this.loadPage())
        })
    }

    findBusForPassenger(id) {
        post(`/passenger/find-bus?passengerId=${id}`)
            .then(response => {
                const {stationFrom, stationCount, route, stationTo} = response.data
                const msg = `Был определен кратчайший маршрут: ${route.title} \n` +
                            `Количество остановок: ${stationCount} \n` +
                            `Остановка отправления: ${stationFrom.title} \n` +
                            `Остановка прибытия: ${stationTo.title} \n`
                this.loadPage()
                this.props.showExceptionPopup(msg)
            })
            .catch(error => {
                this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка поиска автобуса')
            })
    }

    updatePhase = (id, currentState) => {
        var phase
        if (currentState === 'PREPARING') {
            phase = 'phase_to_wait_'
        } else if (currentState === 'WAIT') {
            this.findBusForPassenger(id)
            phase = 'none'
        } else if (currentState === 'BOARDING') {
            post(`/passenger/to-ride?passengerId=${id}`)
                .then(() => this.loadPage())
                .catch(error => {
                    this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                        error.response.data.message : 'Ошибка обновления статуса')
                })
            phase = 'none'
        } else if (currentState === 'RIDE') {
            post(`/passenger/to-unboarding?passengerId=${id}`)
                .then(() => this.loadPage())
                .catch(error => {
                    this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                        error.response.data.message : 'Ошибка обновления статуса')
                })
            phase = 'none'
        } else if (currentState === 'UNBOARDING') {
            del(`/passenger/${id}`)
                .then(() => {
                    this.loadPage()
                    this.props.showExceptionPopup('Пассажир закончил поездку и был удален')
                })
                .catch(error => {
                    this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                        error.response.data.message : 'Ошибка обновления статуса')
                })
            phase = 'none'
        } else {
            return
        }

        this.setState({
            ...this.state,
            phase: phase + id
        })
    }

    loadPage() {
        const size = 10;
        get(`${this.system.urls.main}?limit=${size}&offset=${this.state.page * size}&str=${this.state.str}`)
            .then(res => {
                this.setState({
                    ...this.state,
                    content: res.data
                })
            })
    }

    render() {
        return (
            <>
                <PassengerAdd showExceptionPopup={this.props.showExceptionPopup} pageInstance={this}
                              selector={this.props.selector}/>
                <PassengerUpdate showExceptionPopup={this.props.showExceptionPopup} pageInstance={this}
                                 selector={this.props.selector}/>
                <PassengerStationSelector showExceptionPopup={this.props.showExceptionPopup} pageInstance={this}/>

                <div style={stylePage.div}>
                    <div style={{textAlign: 'right'}}>
                        <TextField label="Поиск по названию" fullWidth onChange={this.handleFilterStr}/>
                    </div>

                    <PassengerTable content={this.state.content} pageInstance={this} selector={this.props.selector}/>

                    <div style={{textAlign: 'right'}}>
                        <Button variant="contained" onClick={() => this.setPhase('create')}
                                style={stylePage.button}>╂</Button>
                        {!this.props.selector &&
                        <Button variant="contained" onClick={() => this.deleteItems()}
                                style={stylePage.button}>╳</Button>
                        }
                        {this.props.selector &&
                        <Button variant="contained" onClick={() => this.props.handleSubmit(this.state.selected)}
                                style={stylePage.button}>OK</Button>
                        }
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <Button variant="contained" onClick={() => this.leftPage()} style={stylePage.button}>←</Button>
                        {this.state.page + 1}
                        <Button variant="contained" onClick={() => this.rightPage()} style={stylePage.button}>→</Button>
                    </div>
                </div>
            </>
        )
    }

    setPhase(phase) {
        this.setState({
            ...this.state,
            phase: phase
        })
    }

    rightPage() {
        const {content} = this.state

        if (content.length < 10) {
            this.props.showExceptionPopup("Показана последняя страница")
            return
        }

        this.setState({
            ...this.state,
            page: this.state.page + 1
        })
    }

    leftPage() {
        if (this.state.page <= 0) {
            this.props.showExceptionPopup("Показана первая страница")
            return
        }

        this.setState({
            ...this.state,
            page: this.state.page - 1
        })
    }

    handleFilterStr = e => {
        const {value} = e.target

        this.setState({
            ...this.state,
            str: value,
            page: 0
        })
    }
}

export const stylePage = {
    div: {
        textAlign: 'left',
        display: 'flex',
        flexDirection: 'column'
    },
    button: {
        margin: '8px 4px',
    }

}

export default PassengerPage;