import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import React from "react";
import Checkbox from "@material-ui/core/Checkbox";
import {Button} from "@material-ui/core";
import Tooltip from "@material-ui/core/Tooltip";

class PassengerTable extends React.Component {

    setSelected = (id) => {
        const selected = this.props.pageInstance.state.selected
        if (selected.includes(id)) {
            this.props.pageInstance.state.selected = selected.filter(i => i !== id)
        } else {
            selected.push(id)
        }
        this.setState({...this.state})
    }

    hasSelected = (id) => {
        const selected = this.props.pageInstance.state.selected
        return selected.includes(id)
    }

    row = (item) => {
        const {id, busId, firstName, lastName, state} = item
        const isSelected = this.hasSelected(id)
        return (
            <TableRow key={item.id}>
                <TableCell>
                    <Checkbox
                        style={{margin: '0 auto'}}
                        color="default"
                        checked={isSelected}
                        onChange={() => this.setSelected(id)}
                    />
                </TableCell>
                <TableCell align="right">{id}</TableCell>
                <TableCell align="right">{lastName}</TableCell>
                <TableCell align="right">{firstName}</TableCell>
                <TableCell align="right">{busId}</TableCell>
                <TableCell align="right">-</TableCell>
                <TableCell align="right">{this.localize(state)}</TableCell>
                {!this.props.selector &&
                <TableCell align="right">
                    <Tooltip
                        title="Переводит пассажира в следующий статус и запрашивает необходимую информацию"
                        aria-label="add"
                    >
                        <Button variant="contained"
                                onClick={() => this.props.pageInstance.updatePhase(id, state)}
                        >
                            ↥
                        </Button>
                    </Tooltip>

                </TableCell>
                }
            </TableRow>
        )
    }

    localize = (str) => {
        if (str === 'PREPARING') {
            return 'Подготовка'
        } else if (str === 'WAIT') {
            return 'Ожидание'
        } else if (str === 'BOARDING') {
            return 'Посадка'
        } else if (str === 'RIDE') {
            return 'Поездка'
        } else if (str === 'UNBOARDING') {
            return 'Высадка'
        }
    }

    render() {
        return (

            <TableContainer component={Paper}>
                <div style={{minHeight: '600px'}}>
                    <Table size="small" aria-label="a dense table">
                        <TableHead>
                            <TableRow>
                                <TableCell align="right"/>
                                <TableCell align="right">Id</TableCell>
                                <TableCell align="right">Фамилия</TableCell>
                                <TableCell align="right">Имя</TableCell>
                                <TableCell align="right">Автобус</TableCell>
                                <TableCell align="right">Маршрут</TableCell>
                                <TableCell align="right">Статус</TableCell>
                                {!this.props.selector && <TableCell align="right">Изменить статус</TableCell>}
                            </TableRow>
                        </TableHead>
                        <TableBody>

                            {this.props.content && this.props.content.map(i => this.row(i))}

                        </TableBody>
                    </Table>
                </div>
            </TableContainer>
        )
    }
}

export default PassengerTable