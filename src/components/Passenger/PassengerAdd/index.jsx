import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {post} from "../../../service/api";

class PassengerAdd extends React.Component {

    constructor(props) {
        super(props);

        this.state = this.defaultState()
    }

    defaultState() {
        return {firstName: '', lastName: ''}
    }

    close = () => {
        this.props.pageInstance.setState({
            ...this.props.pageInstance.state,
            phase: 'none'
        })
        this.setState({
            ...this.state,
            ...this.defaultState()
        })
        this.props.pageInstance.loadPage()
    }

    handleChange = e => {
        const {name, value} = e.target

        if (name === 'firstName') {
            if (value.length > 63) {
                this.props.showExceptionPopup("Превышен лимит символов")
                return
            }

            this.setState({
                ...this.state,
                firstName   : value
            })
        }

        if (name === 'lastName') {
            if (value.length > 63) {
                this.props.showExceptionPopup("Превышен лимит символов")
                return
            }

            this.setState({
                ...this.state,
                lastName: value
            })
        }
    }

    handleSubmit = () => {
        const {firstName, lastName} = this.state
        const data = {
            firstName, lastName
        }
        post('/passenger', data)
            .then(() => this.close())
            .catch(error => {
                this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка добавления автобуса')
            })
    }

    render() {
        const phase = this.props.pageInstance.state.phase;
        return (
            <>
                <Dialog open={phase === 'create'} onClose={this.close}>
                    <DialogContent style={{width: '500px'}}>
                        <h2>Добавление пассажира</h2>
                        <TextField value={this.state.firstName}
                                   name="firstName"
                                   label="Имя пассажира"
                                   onChange={this.handleChange}
                                   style={{width: '100%'}}
                        />
                        <TextField value={this.state.lastName}
                                   name="lastName"
                                   label="Фамилия пассажира"
                                   onChange={this.handleChange}
                                   style={{width: '100%'}}
                        />
                        <br/>
                        <br/>
                        <Button variant="contained" onClick={this.handleSubmit}>Добавить</Button>
                        <br/>
                        <br/>
                    </DialogContent>
                </Dialog>
            </>
        )

    }
}

export default PassengerAdd