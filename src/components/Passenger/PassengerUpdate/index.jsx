import React from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import {get, patch} from "../../../service/api";

class PassengerUpdate extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            routeId: 'загрузка',
            id: null,
            stations: []
        }
    }


    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.id !== this.extractId()) {
            this.loadData()
        }
    }

    extractId = () => {
        const phase = this.props.pageInstance.state.phase
        return parseInt(phase.replace("update_", ''))
    }

    loadData = () => {
        const id = this.extractId()

        if (isNaN(id))
            return

        get(`/passenger/${id}`)
            .then(response => {
                const {id, routeId} = response.data
                this.setState({
                    ...this.state,
                    id,
                    routeId
                })
            })
    }

    close = () => {
        this.props.pageInstance.setState({
            ...this.props.pageInstance.state,
            phase: 'none'
        })
        this.props.pageInstance.loadPage()
    }

    handleChange = e => {
        const {name, value} = e.target

        if (value !== '' && value.match(/^[0-9]+$/) === null) {
            return
        }

        if (name === 'routeId') {
            if (value.length > 7) {
                this.props.showExceptionPopup("Превышен лимит символов")
                return
            }

            this.setState({
                ...this.state,
                routeId: value
            })
        }
    }

    handleSubmit = () => {
        const data = {
            routeId: this.state.routeId
        }
        patch(`/passenger/${this.state.id}`, data)
            .then(() => this.close())
            .catch(error => {
                this.props.showExceptionPopup(error.response.data && error.response.data.message ?
                    error.response.data.message : 'Ошибка изменения автобуса')
            })
    }

    render() {
        const phase = this.props.pageInstance.state.phase;
        return (
            <>
                <Dialog open={phase.startsWith('update')} onClose={this.close}>
                    <DialogContent style={{width: '500px'}}>
                        <h2>Изменение маршрута</h2>
                        <TextField value={this.state.routeId}
                                   name="routeId"
                                   label="Название маршрута"
                                   onChange={this.handleChange}
                                   style={{width: '100%'}}
                        />
                        <br/>
                        <br/>
                        <Button variant="contained" onClick={this.handleSubmit}>Изменить</Button>
                        <br/>
                        <br/>
                    </DialogContent>
                </Dialog>
            </>
        )

    }
}

export default PassengerUpdate