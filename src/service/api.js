import axios from 'axios'

const API_URL = 'http://api.bus.toppink.ru:80'

export function patch(endpoint, data, headers = {}) {
    const config = {
        headers: {
            ...headers,
        },
    }
    return axios.patch(API_URL + endpoint, data, config)
}

export function post(endpoint, data = {}, headers = {}) {
    const config = {
        headers: {
            ...headers,
        }
    }
    return axios.post(API_URL + endpoint, data, config)
}

export function get(endpoint, data = {}, headers = {}) {
    const config = {
        headers: {
            ...headers,
        },
        data: data
    }
    return axios.get(API_URL + endpoint, config)
}

export function del(endpoint, data = {}, headers = {}) {
    const config = {
        headers: {
            ...headers,
        },
        data: data
    }
    return axios.delete(API_URL + endpoint, config)
}

export {API_URL}